# Mock Moodle

This application pretends that it's a Moodle site's API and remembers some
state so it can assert things like you can't add someone to a group that hasn't
been added to a course.

## Install the Application

Use composer to install dependencies.

```bash
$ composer install
```

## Usage

Point a web server at index.php or use the php built-in server.

```bash
$ composer start
> php -S localhost:8080 -t public
```

Send requests to `/api?wsfunction=foo` where foo is the
name of the Moodle web service.

```
POST localhost:8080/api?wsfunction=core_users_create_user

... Some request to create a user.
```

Responses will be a standard response for that web service.

```
{
  "id": 1234,
  "some other stuff": "huzzah!"
}
```

Or an error!

```
{
  "exception": "invalid parameter exception",
  "message": "Invalid parameter value detected.",
  "errorcode": "invalidparameter",
  // Moodle doesn't like to explain its errors but that doesn't mean we can't.
  "debuginfo": [
    "User 123 must first be enrolled in course 456."
  ]
}
```

State of the application is saved between requests. Set the path to a file
where the state will be stored in settings.php.

```
'state' => [
    // State is saved here between requests.
    'path' => '../state.json',
],
```

To reset the state, make a DELETE request to `/api/state` or use `composer reset-state`

```
$ composer reset-state
> curl -s localhost:8080/api/state --header "Accept: application/json"  --header "Content-Type: application/JSON" --request DELETE
```

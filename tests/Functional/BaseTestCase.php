<?php

namespace AcroMedia\MockMoodle\Tests\Functional;

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\Environment;

/**
 * This is an example class that shows how you could set up a method that
 * runs the application. Note that it doesn't cover all use-cases and is
 * tuned to the specifics of this skeleton app, so if your needs are
 * different, you'll need to change it.
 */
class BaseTestCase extends \PHPUnit_Framework_TestCase
{
    /**
     * Use middleware when running application?
     *
     * @var bool
     */
    protected $withMiddleware = true;

    /**
     * Process the application given a request method and URI
     *
     * @param string $requestMethod the request method (e.g. GET, POST, etc.)
     * @param string $requestUri the request URI
     * @param array|object|null $requestData the request data
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function runApp($requestMethod, $requestUri, $requestData = null)
    {
        // Create a mock environment for testing with
        $environment = Environment::mock(
            [
                'HTTP_ACCEPT' => 'application/json',
                'HTTP_CONTENT_TYPE' => 'application/json',
                'REQUEST_METHOD' => $requestMethod,
                'REQUEST_URI' => $requestUri
            ]
        );

        // Set up a request object based on the environment
        $request = Request::createFromEnvironment($environment);

        // Add request data, if it exists
        if (isset($requestData)) {
            $request = $request->withParsedBody($requestData);
        }

        // Set up a response object
        $response = new Response();

        // Use the application settings
        $settings = require __DIR__ . '/../../src/settings.php';
        $settings['settings']['state']['path'] = $settings['settings']['state']['test_path'];

        // Instantiate the application
        $app = new App($settings);

        // Set up dependencies
        require __DIR__ . '/../../src/dependencies.php';

        // Register middleware
        if ($this->withMiddleware) {
            require __DIR__ . '/../../src/middleware.php';
        }

        // Register routes
        require __DIR__ . '/../../src/routes.php';

        // Process the application
        $response = $app->process($request, $response);

        // Return the response
        return $response;
    }

    /**
     * Process the application given a request method and URI that should return JSON.
     *
     * @param string $requestMethod the request method (e.g. GET, POST, etc.)
     * @param string $requestUri the request URI
     * @param array|object|null $requestData the request data
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function runForJSON($requestMethod, $requestUri, $requestData = null)
    {
        $response = $this->runApp($requestMethod, $requestUri, $requestData);
        $this->assertValidJSON((string) $response->getBody());
        return $response;
    }

    /**
     * Asserts that the given string is valid JSON.
     *
     * @param $string
     */
    public function assertValidJSON($string): void
    {
        $json = json_decode($string, true);
        $this->assertNotNull($json, 'Response contains valid JSON.');
    }

    /**
     * Assert the current system state.
     *
     * @param string $state
     *   The expected state (as JSON).
     */
    public function assertState($state): void
    {
        $response = $this->runApp('GET', '/api/state');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals($state, (string) $response->getBody());
    }

    /**
     * Get the URI for a web service function.
     *
     * @param string $method
     * @return string
     */
    protected function uri(string $method): string
    {
        return "/api?wsfunction={$method}";
    }

    /**
     * Reset application state between tests.
     */
    public function tearDown()
    {
        parent::tearDown();
        $response = $this->runApp('DELETE', '/api/state');
        $this->assertEquals(200, $response->getStatusCode(), 'Reset state.');
    }
}

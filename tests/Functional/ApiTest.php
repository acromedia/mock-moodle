<?php

namespace AcroMedia\MockMoodle\Tests\Functional;

/**
 * Tests behaviour of the API.
 */
class ApiTest extends BaseTestCase
{
    /**
     * Test that the echo route returns exactly what it was given.
     */
    public function testEcho()
    {
        $response = $this->runApp('POST', $this->uri('echo'), ['echo' => 'foo']);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("foo\n", (string)$response->getBody());
    }

    /**
     * Tests the application with state saved between requests.
     */
    public function testApp()
    {
        $this->addMembersThatDontExist();
        $this->createUser();
        $this->createCourse();
        $this->createGroupInCourseThatDoesntExist();
        $this->createGroup();
        $this->addMembersNotInCourse();
        $this->enrolUser();
        $this->addMembers();
        $this->assertState('{"groups":{"1":["1"]},"courses":{"1":["1"]},"course_groups":{"1":["1"]},"users":{"1":1,"2":2}}');
        $this->deleteUserShouldFail('3');
        $this->createCategories();
        $this->createGroupings();
        $this->assignGroupingShouldFail('2');
        $this->assignGrouping('1');
        $this->createUser();
        $this->deleteGroupMemberShouldFail('1', '2');
        $this->deleteGroupMember('1', '1');
        $this->deleteUser('2');
        $this->assignUserShouldFail('99999', '99999');
        $this->assignUser('1', '1');
        $this->addCohortMemberShouldFail('99999', '1');
        $this->addCohortMember('1', '1');
        $this->assignUserShouldFail('99999', '99999');
        $this->assignUser('1', '1');
        $this->assignManagerShouldFail('99999', '99999');
        $this->assignManager('1', '1');
    }

    /**
     * Ensure we can't add members to a group if they haven't been created yet.
     */
    public function addMembersThatDontExist()
    {
        $data['members'][] = [
            'userid' => '99999',
            'groupid' => '99999',
        ];
        $response = $this->runForJSON('POST', $this->uri('core_group_add_group_members'), $data);

        $response_body = (string) $response->getBody();
        $this->assertEquals(400, $response->getStatusCode(), 'Could not add group members that do not exist.');
        $this->assertContains('Invalid parameter value detected.', $response_body);
        $this->assertContains('99999 is not a valid group.', $response_body);
        $this->assertContains('99999 is not a valid user.', $response_body);
    }

    /**
     * Create a single user.
     */
    public function createUser()
    {
        $data['users'][] = [];
        $response = $this->runForJSON('POST', $this->uri('lambda_sso_create_user'), $data);
        $response_body = (string) $response->getBody();
        $this->assertEquals(200, $response->getStatusCode(), 'Created a user.');
        $this->assertNotContains('Invalid parameter value detected.', $response_body);
        $this->assertContains('id', $response_body);

        $response = $this->runForJSON('POST', $this->uri('core_user_create_users'), $data);
        $response_body = (string) $response->getBody();
        $this->assertEquals(200, $response->getStatusCode(), 'Created a user.');
        $this->assertNotContains('Invalid parameter value detected.', $response_body);
        $this->assertContains('id', $response_body);
    }

    /**
     * Create a single course.
     */
    public function createCourse()
    {
        $data['courses'][] = ['shortname' => 'foobar'];
        $response = $this->runForJSON('POST', $this->uri('core_course_create_courses'), $data);

        $response_body = (string) $response->getBody();
        $this->assertEquals(200, $response->getStatusCode(), 'Created a course.');
        $this->assertNotContains('Invalid parameter value detected.', $response_body);
        $this->assertContains('id', $response_body, 'Course ID returned.');
        $this->assertContains('foobar', $response_body, 'Course shortname returned.');
    }

    /**
     * Attempt to create a group without a valid course.
     */
    public function createGroupInCourseThatDoesntExist()
    {
        $data['groups'][] = [
            'courseid' => '99999',
        ];
        $response = $this->runForJSON('POST', $this->uri('core_group_create_groups'), $data);

        $response_body = (string) $response->getBody();
        $this->assertEquals(400, $response->getStatusCode(), 'Could not create a group with an invalid course.');
        $this->assertContains('Invalid parameter value detected.', $response_body);
        $this->assertContains('99999 is not a valid course.', $response_body);
    }

    /**
     * Create a single group.
     */
    public function createGroup()
    {
        $data['groups'][] = [
            'courseid' => '1',
        ];
        $response = $this->runForJSON('POST', $this->uri('core_group_create_groups'), $data);

        $response_body = (string) $response->getBody();
        $this->assertEquals(200, $response->getStatusCode(), 'Created a group.');
        $this->assertContains('id', $response_body, 'Group ID returned.');
    }

    /**
     * Add members to a group before they're in a course.
     */
    public function addMembersNotInCourse()
    {
        $data['members'][] = [
            'userid' => '1',
            'groupid' => '1',
        ];
        $response = $this->runForJSON('POST', $this->uri('core_group_add_group_members'), $data);

        $response_body = (string) $response->getBody();
        $this->assertEquals(400, $response->getStatusCode(), 'Could not add group members not enrolled in a course.');
        $this->assertContains('User 1 must first be enrolled in course 1.', $response_body);
    }

    /**
     * Attempt to enrol users without a valid course.
     */
    public function enrolUserInCourseThatDoesntExist()
    {
        $data['enrolments'][] = [
            'userid' => '99999',
            'courseid' => '99999',
        ];

        $response = $this->runForJSON('POST', $this->uri('enrol_manual_enrol_users'), $data);

        $response_body = (string) $response->getBody();
        $this->assertEquals(400, $response->getStatusCode(), 'Could not enrol users in a course that does not exist.');
        $this->assertContains('Invalid parameter value detected.', $response_body);
        $this->assertContains('99999 is not a valid course.', $response_body);
        $this->assertContains('99999 is not a valid user.', $response_body);
    }

    /**
     * Enrol a user in a course.
     */
    public function enrolUser()
    {
        $data['enrolments'][] = [
            'userid' => '1',
            'courseid' => '1',
            'roleid' => '1',
        ];

        $response = $this->runApp('POST', $this->uri('enrol_manual_enrol_users'), $data);

        $response_body = (string) $response->getBody();
        $this->assertEquals(200, $response->getStatusCode(), 'Enrolled a user in a course.');
        $this->assertEmpty($response_body);
    }

    /**
     * Add members to a group successfully.
     */
    public function addMembers()
    {
        $data['members'][] = [
            'userid' => '1',
            'groupid' => '1',
            'roleid' => '1',
        ];
        $response = $this->runApp('POST', $this->uri('core_group_add_group_members'), $data);

        $response_body = (string) $response->getBody();
        $this->assertEquals(200, $response->getStatusCode(), 'Added members to a group.');
        $this->assertEmpty($response_body, 'Response is empty.');
    }

    /**
     * Delete a user.
     *
     * @param string $id
     *   The ID of a user.
     */
    public function deleteUser($id)
    {
        $data['userids'][] = $id;
        $response = $this->runApp('POST', $this->uri('core_user_delete_users'), $data);
        $this->assertEquals(200, $response->getStatusCode(), 'Deleted a user.');
    }

    /**
     * Try deleting a user.
     *
     * @param string $id
     *   The ID of a user that doesn't exist.
     */
    public function deleteUserShouldFail($id)
    {
        $data['userids'][] = $id;
        $response = $this->runApp('POST', $this->uri('core_user_delete_users'), $data);
        $this->assertEquals(400, $response->getStatusCode(), 'Failed to delete a user.');
        $this->assertContains(sprintf('%s is not a valid user.', $id), (string) $response->getBody());
    }

    /**
     * Create categories and make sure the IDs are returned.
     */
    public function createCategories()
    {
        // Create three categories (no data necessary; none is used).
        $data['categories'] = [[],[],[]];
        $response = $this->runForJSON('POST', $this->uri('core_course_create_categories'), $data);
        $this->assertEquals(200, $response->getStatusCode(), 'Created categories');
        $response_body = (string) $response->getBody();
        $this->assertContains('0', $response_body);
        $this->assertContains('1', $response_body);
        $this->assertContains('2', $response_body);
    }

    /**
     * Create groupings and make sure the IDs are returned.
     */
    public function createGroupings()
    {
        // Create three groupings (no data necessary; none is used).
        $data['groupings'] = [[],[],[]];
        $response = $this->runForJSON('POST', $this->uri('core_group_create_groupings'), $data);
        $this->assertEquals(200, $response->getStatusCode(), 'Created groupings.');
        $response_body = (string) $response->getBody();
        $this->assertContains('0', $response_body);
        $this->assertContains('1', $response_body);
        $this->assertContains('2', $response_body);
    }

    /**
     * Assign a group to a grouping.
     *
     * @param string $id
     *   The ID of a group.
     */
    public function assignGrouping(string $id)
    {
        $data['assignments'][] = ['groupid' => $id];
        $response = $this->runApp('POST', $this->uri('core_group_assign_grouping'), $data);
        $this->assertEquals(200, $response->getStatusCode(), 'Assigned a group to a grouping.');
    }

    /**
     * Assign a group to a grouping.
     *
     * @param string $id
     *   The ID of a group that doesn't exist.
     */
    public function assignGroupingShouldFail(string $id)
    {
        $data['assignments'][] = ['groupid' => $id];
        $response = $this->runApp('POST', $this->uri('core_group_assign_grouping'), $data);
        $this->assertEquals(400, $response->getStatusCode(), 'Failed to assign a group to a groupind.');
        $this->assertContains(sprintf('%s is not a valid group.', $id), (string) $response->getBody());
    }

    /**
     * Remove a member from a group.
     *
     * @param string $groupId
     * @param string $userId
     */
    public function deleteGroupMember(string $groupId, string $userId)
    {
        $data['members'][] = ['groupid' => $groupId, 'userid' => $userId];
        $response = $this->runApp('POST', $this->uri('core_group_delete_group_members'), $data);
        $this->assertEquals(200, $response->getStatusCode(), 'Removed group members.');
    }

    /**
     * Fail to a member from a group.
     *
     * @param string $groupId
     * @param string $userId
     *   A user that is not a member of the given group.
     */
    public function deleteGroupMemberShouldFail(string $groupId, string $userId)
    {
        $data['members'][] = ['groupid' => $groupId, 'userid' => $userId];
        $response = $this->runApp('POST', $this->uri('core_group_delete_group_members'), $data);
        $this->assertEquals(400, $response->getStatusCode(), 'Failed to remove group members.');
        $this->assertContains(sprintf('User %s must first be in group %s.', $userId, $groupId), (string) $response->getBody());
    }

    /**
     * Assign a user to an organization.
     *
     * @param string $userId
     * @param string $organizationId
     */
    public function assignUser(string $userId, string $organizationId)
    {
        $data = ['user_id' => $userId, 'organization_id' => $organizationId];
        $response = $this->runForJSON('POST', $this->uri('local_lambdaws_assign_user'), $data);
        $this->assertEquals(200, $response->getStatusCode(), 'Assigned a user to an organization.');
        $response_body = (string) $response->getBody();
        $this->assertContains('"id":"1"', $response_body);
    }

    /**
     * Assign a user that doesn't exist to an organization.
     *
     * @param string $userId
     * @param string $organizationId
     */
    public function assignUserShouldFail(string $userId, string $organizationId)
    {
        $data = ['user_id' => $userId, 'organization_id' => $organizationId];
        $response = $this->runForJSON('POST', $this->uri('local_lambdaws_assign_user'), $data);
        $this->assertEquals(400, $response->getStatusCode(), 'Failed to assign a user.');
        $response_body = (string) $response->getBody();
        $this->assertContains(sprintf('%s is not a valid user.', $userId), $response_body);
    }

    /**
     * Assign a user to a cohort.
     *
     * @param string $userId
     * @param string $cohortId
     */
    public function addCohortMember(string $userId, string $cohortId)
    {
        $data['members'][] = [
            'cohorttype' => [
                'type' => 'id',
                'value' => $cohortId,
            ],
            'usertype' => [
                'type' => 'id',
                'value' => $userId,
            ],
        ];
        $response = $this->runForJSON('POST', $this->uri('core_cohort_add_cohort_members'), $data);
        $this->assertEquals(200, $response->getStatusCode(), 'Assigned a user to a cohort.');
        $response_body = (string) $response->getBody();
        $this->assertContains('"warnings":[]', $response_body);
    }

    /**
     * Assign a user that doesn't exist to a cohort.
     *
     * @param string $userId
     * @param string $cohortId
     */
    public function addCohortMemberShouldFail(string $userId, string $cohortId)
    {
        $data['members'][] = [
            'cohorttype' => [
                'type' => 'id',
                'value' => $cohortId,
            ],
            'usertype' => [
                'type' => 'id',
                'value' => $userId,
            ],
        ];
        $response = $this->runForJSON('POST', $this->uri('core_cohort_add_cohort_members'), $data);
        $this->assertEquals(400, $response->getStatusCode(), 'Failed to assign a user to a cohort.');
        $response_body = (string) $response->getBody();
        $this->assertContains(sprintf('%s is not a valid user.', $userId), $response_body);
    }

    /**
     * Remove a user from a cohort.
     *
     * @param string $userId
     * @param string $cohortId
     */
    public function removeCohortMember(string $userId, string $cohortId)
    {
        $data['members'][] = [
            'cohortid' => $cohortId,
            'userid' => $userId,
        ];
        $response = $this->runApp('POST', $this->uri('core_cohort_delete_cohort_members'), $data);
        $this->assertEquals(200, $response->getStatusCode(), 'Removed a user from a cohort.');
        $response_body = (string) $response->getBody();
        $this->assertEmpty($response_body, 'Received an empty response.');
    }

    /**
     * Remove a user that doesn't exist from a cohort.
     *
     * @param string $userId
     * @param string $cohortId
     */
    public function removeCohortMemberShouldFail(string $userId, string $cohortId)
    {
        $data['members'][] = [
            'cohortid' => $cohortId,
            'userid' => $userId,
        ];
        $response = $this->runApp('POST', $this->uri('core_cohort_delete_cohort_members'), $data);
        $this->assertEquals(400, $response->getStatusCode(), 'Failed to remove a user from a cohort.');
        $response_body = (string) $response->getBody();
        $this->assertContains(sprintf('%s is not a valid user.', $userId), $response_body);
    }

    /**
     * Assign a manager to a user.
     *
     * @param string $userId
     * @param string $managerId
     */
    public function assignManager(string $userId, string $managerId)
    {
        $data = ['user_id' => $userId, 'manager_id' => $managerId];
        $response = $this->runForJSON('POST', $this->uri('local_lambdaws_assign_manager'), $data);
        $this->assertEquals(200, $response->getStatusCode(), 'Assigned a manager to a user.');
        $response_body = (string) $response->getBody();
        $this->assertContains('"id":"1"', $response_body);
    }

    /**
     * Assign a manager that doesn't exist to a user that doesn't exist.
     *
     * @param string $userId
     * @param string $managerId
     */
    public function assignManagerShouldFail(string $userId, string $managerId)
    {
        $data = ['user_id' => $userId, 'manager_id' => $managerId];
        $response = $this->runForJSON('POST', $this->uri('local_lambdaws_assign_manager'), $data);
        $this->assertEquals(400, $response->getStatusCode(), 'Failed to assign a manager to a user.');
        $response_body = (string) $response->getBody();
        $this->assertContains(sprintf('%s is not a valid user.', $userId), $response_body);
        $this->assertContains(sprintf('%s is not a valid user.', $managerId), $response_body);
    }
}

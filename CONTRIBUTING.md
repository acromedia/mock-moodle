# Contributing

Learn about the [slim framework](http://www.slimframework.com) to understand
how the application runs.

Follow PSR-2 code standards.

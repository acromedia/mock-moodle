# State

Application state is saved between requests in this directory - or a location of
your choosing if you configure a different path in settings.php.

The state file must be writable by the web server.

<?php

namespace AcroMedia\MockMoodle;

use DavidePastore\Slim\Views\MarkdownRenderer;
use Psr\Container\ContainerInterface;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Monolog\Handler\StreamHandler;
use Slim\Http\Environment;
use Slim\Http\Uri;
use Slim\Views\Twig;
use Slim\Views\TwigExtension;
use AcroMedia\MockMoodle\State\MoodleState;

if (!defined('DIR')) {
    define('DIR', __DIR__);
}

/** @var \Slim\App $app */
// DIC configuration
$container = $app->getContainer();

// Twig renderer.
$container['view'] = function (ContainerInterface $c) {
    $settings = $c->get('settings')['view'];
    $args = $settings['cache'] ? ['cache' => $settings['cache_path']] : [];
    $view = new Twig($settings['template_path'], $args);

    $router = $c->get('router');
    $uri = Uri::createFromEnvironment(new Environment($_SERVER));
    $view->addExtension(new TwigExtension($router, $uri));

    return $view;
};

// Markdown renderer.
$container['markdown'] = function (ContainerInterface $c) {
    return new MarkdownRenderer();
};

// Monolog
$container['logger'] = function (ContainerInterface $c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Logger($settings['name']);
    $logger->pushProcessor(new UidProcessor());
    $logger->pushHandler(new StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

// Moodle application state.
$container['state'] = function (ContainerInterface $c) {
    return new MoodleState($c->get('settings')['state']['path']);
};

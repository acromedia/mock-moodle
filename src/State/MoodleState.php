<?php

namespace AcroMedia\MockMoodle\State;

use AcroMedia\MockMoodle\Exception\InvalidStateException;

/**
 * Stores state about the Moodle application.
 *
 * State is a series of lists:
 * - Users.
 * - Courses and their users.
 * - Courses and their groups.
 * - Groups and their users.
 */
final class MoodleState
{

    /**
     * The path to the application state in the file system.
     *
     * @var string
     */
    private $statePath;

    /**
     * The application state.
     *
     * @var array
     */
    private $state;

    /**
     * @param string $path
     *   The path to the existing application state.
     *
     * @throws InvalidStateException
     */
    public function __construct(string $path)
    {
        $this->statePath = DIR . "/{$path}";
        $this->load($this->statePath);
    }

    /**
     * Load the application state from disk.
     *
     * @param string $path
     *   The path to the existing application state.
     *
     * @return array
     *
     * @throws InvalidStateException
     */
    private function load(string $path)
    {
        if (!file_exists($path) && is_writable(dirname($path))) {
            // Write an empty state.
            file_put_contents($path, '{}');
        }

        if (!file_exists($path)) {
            throw new \InvalidArgumentException(sprintf('%s does not exist', $path));
        }

        $state = file_get_contents($path);
        $state = json_decode($state, true);
        if ($state === null) {
            throw new InvalidStateException('The state is not valid JSON.');
        }

        $this->state = $this->inflateState($state);
        return $state;
    }

    /**
     * Un-pack json-decoded state into value objects.
     *
     * @param array $state
     * @return array
     *   The same state, where entities in the state are turned in to value objects.
     */
    private function inflateState(array $state): array
    {
        // Collection provides an array map that doesn't clobber array keys.
        $state['groups'] = collect($state['groups'] ?? []);
        $state['courses'] = collect($state['courses'] ?? []);
        $state['course_groups'] = collect($state['course_groups'] ?? []);
        $state['users'] = $state['users'] ?? [];

        $state['groups'] = $state['groups']->map(function ($users, $id) {
            return new Group($id, $users);
        })->toArray();

        $state['courses'] = $state['courses']->map(function ($users, $id) {
            return new Course($id, $users);
        })->toArray();

        // Re-use each Group's value object we created before in each list of groups.
        $state['course_groups'] = $state['course_groups']->map(function ($groups) use ($state) {
            return collect($groups)->map(function ($group) use ($state) {
                return $state['groups'][$group];
            });
        })->toArray();

        return $state;
    }

    /**
     * Resets the current state.
     */
    public function reset(): void
    {
        $this->state = [];
    }

    /**
     * Save the application state to disk.
     *
     * @throws InvalidStateException
     */
    public function save(): void
    {
        $state = $this->getJSON();
        $written = file_put_contents($this->statePath, $state);
        if (!$written) {
            throw new InvalidStateException(sprintf('The state could not be saved to %s', $this->statePath));
        }
    }

    /**
     * Get the state of entities of the given type.
     *
     * @param string $type
     *   A type of moodle entity.
     * @return array
     *   An array of information about the application state.
     */
    public function get(string $type): array
    {
        return $this->state[$type] ?? [];
    }

    /**
     * Get the current state as JSON.
     */
    public function getJSON()
    {
        $state = $this->state;
        // Avoid duplicating group data when saving by saving only the group ID in the course->groups map.
        if (!empty($state['course_groups'])) {
            $state['course_groups'] = collect($state['course_groups'])->map(function ($groups) {
                return collect($groups)->map(function ($group) {
                    /** @var Group $group */
                    return $group->id();
                });
            });
        }
        return json_encode($state);
    }

    /**
     * Get all the groups in the application.
     *
     * @return Group[]
     *   A list of Groups.
     */
    public function groups(): array
    {
        return $this->get('groups');
    }

    /**
     * Get a single group by ID.
     *
     * @param string $id
     * @return Group
     *
     * @throws \InvalidArgumentException
     */
    public function group(string $id): Group
    {
        if (!isset($this->groups()[$id])) {
            throw new \InvalidArgumentException(sprintf('%s is not a valid group.', $id));
        }

        return $this->groups()[$id];
    }

    /**
     * Get all the courses in the application.
     *
     * @return Course[]
     *   A list of Courses.
     */
    public function courses(): array
    {
        return $this->get('courses');
    }

    /**
     * Get a single course by ID.
     *
     * @param string $id
     * @return Course
     *
     * @throws \InvalidArgumentException
     */
    public function course(string $id): Course
    {
        if (!isset($this->courses()[$id])) {
            throw new \InvalidArgumentException(sprintf('%s is not a valid course.', $id));
        }

        return $this->courses()[$id];
    }

    /**
     * Get the groups that are in each course.
     *
     * @return array
     *   An array keyed by course Ids where each value is a list of Groups.
     */
    public function coursesGroups(): array
    {
        return $this->get('course_groups');
    }

    /**
     * Get the groups in a course.
     *
     * @param string $id
     * @return Group[]
     *
     * @throws \InvalidArgumentException
     */
    public function courseGroups(string $id): array
    {
        if (!isset($this->courses()[$id])) {
            throw new \InvalidArgumentException(sprintf('%s is not a valid course.', $id));
        }

        return $this->get('course_groups')[$id];
    }

    /**
     * Get the course a group is in.
     *
     * @param string $id
     *   The ID of a group.
     * @return Course
     *   The group's course.
     *
     * @throws \InvalidArgumentException
     */
    public function groupCourse(string $id): Course
    {
        foreach ($this->get('course_groups') as $courseId => $groups) {
            /** @var Group $group */
            foreach ($groups as $group) {
                if ($id === $group->id()) {
                    return $this->course($courseId);
                }
            }
        }
        throw new \InvalidArgumentException(sprintf('group %s does not have a course', $id));
    }

    /**
     * Add a new user to the state.
     *
     * @return string
     *   The ID of the user new.
     */
    public function createUser(): string
    {
        $users = $this->users();
        $keys = \array_keys($users);
        $last = $users[end($keys)] ?? 0;
        $new = $last + 1;
        $this->state['users'][$new] = $new;
        return $new;
    }

    /**
     * Delete a user.
     *
     * @param string $id
     */
    public function deleteUser(string $id): void
    {
        unset($this->state['users'][$id]);
    }


    /**
     * Add a new course to the state.
     *
     * @param string|null $shortname
     *   An optional name for the course.
     *
     * @return Course
     *   The new course.
     */
    public function createCourse(?string $shortname = null): Course
    {
        $courses = $this->courses();
        $keys = \array_keys($courses);
        $next = (\end($keys) ?? 0) + 1;
        $course = new Course($next, [], $shortname);
        $this->state['courses'][$next] = $course;
        return $course;
    }

    /**
     * Add a new group to the state.
     *
     * @param Course $course
     *   The course to create the group in.
     *
     * @return Group
     *   The new Group.
     */
    public function createGroup(Course $course): Group
    {
        $groups = $this->groups();
        $keys = \array_keys($groups);
        $next = (\end($keys) ?? 0) + 1;
        $group = new Group($next, []);
        $this->state['groups'][$next] = $group;
        $this->state['course_groups'][$course->id()][] = $group;
        return $group;
    }

    /**
     * Get all the users in the application.
     *
     * @return string[]
     *   A list of user IDs.
     */
    public function users(): array
    {
        return $this->get('users');
    }
}

<?php

namespace AcroMedia\MockMoodle\State\Constraint;

use AcroMedia\MockMoodle\State\Group;

/**
 * Determines whether a user is in a group.
 */
final class UserInGroup extends ConstraintBase
{
    /**
     * Determine whether a user is in a course.
     *
     * @param string $user
     *   The ID of a user.
     * @param Group $group
     * @return bool
     */
    public function satisfied(string $user, Group $group): bool
    {
        return $group->has($user);
    }
}

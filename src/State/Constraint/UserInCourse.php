<?php

namespace AcroMedia\MockMoodle\State\Constraint;

use AcroMedia\MockMoodle\State\Course;

/**
 * Determines whether a user is in a group.
 */
final class UserInCourse extends ConstraintBase
{
    /**
     * Determine whether a user is in a course.
     *
     * @param string $user
     *   The ID of a user.
     * @param Course $course
     * @return bool
     */
    public function satisfied(string $user, Course $course): bool
    {
        return $course->has($user);
    }
}

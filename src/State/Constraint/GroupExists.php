<?php

namespace AcroMedia\MockMoodle\State\Constraint;

/**
 * Determine if a group exists.
 */
final class GroupExists extends EntityExists
{
    protected function type(): string
    {
        return 'groups';
    }
}

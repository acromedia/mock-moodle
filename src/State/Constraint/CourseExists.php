<?php

namespace AcroMedia\MockMoodle\State\Constraint;

/**
 * Determine if a course exists.
 */
final class CourseExists extends EntityExists
{
    protected function type(): string
    {
        return 'courses';
    }
}

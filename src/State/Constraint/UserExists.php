<?php

namespace AcroMedia\MockMoodle\State\Constraint;

/**
 * Determine if a user exists.
 */
final class UserExists extends EntityExists
{
    protected function type(): string
    {
        return 'users';
    }
}

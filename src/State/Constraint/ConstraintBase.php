<?php

namespace AcroMedia\MockMoodle\State\Constraint;

use AcroMedia\MockMoodle\State\MoodleState;

/**
 * Common functions for constraints.
 */
abstract class ConstraintBase
{
    /**
     * @var MoodleState
     */
    protected $state;

    /**
     * @param MoodleState $state
     *   The application state.
     */
    public function __construct(MoodleState $state)
    {
        $this->state = $state;
    }
}

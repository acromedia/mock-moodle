<?php

namespace AcroMedia\MockMoodle\State\Constraint;

abstract class EntityExists extends ConstraintBase
{
    abstract protected function type(): string;

    public function satisfied(string $id): bool
    {
        return array_key_exists($id, $this->state->get($this->type()));
    }
}

<?php

namespace AcroMedia\MockMoodle\State;

/**
 * Value object for a course.
 */
final class Course implements \JsonSerializable
{
    /**
     * A list of user IDs.
     *
     * @var array
     */
    private $users;

    /**
     * @var string
     */
    private $shortname;

    /**
     * The course's ID.
     *
     * @var string
     */
    private $id;

    public function __construct(string $id, array $users, $shortname = null)
    {
        $this->id = $id;
        $this->users = $users;
        $this->shortname = $shortname;
    }

    /**
     * Get the course's ID.
     *
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * Whether a user is in the group.
     *
     * @param string $id
     * @return bool
     */
    public function has(string $id): bool
    {
        return in_array($id, $this->users, true);
    }

    /**
     * Add a user to the course.
     *
     * @param string $id
     * @return Course
     */
    public function add(string $id): self
    {
        $this->users[] = $id;
        return $this;
    }

    /**
     * Get the course shortname.
     *
     * NULL for any course not created in the same requests since we don't store the name.
     *
     * @return string|null
     */
    public function shortname(): ?string
    {
        return $this->shortname;
    }

    public function jsonSerialize()
    {
        return $this->users;
    }
}

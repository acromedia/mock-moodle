<?php

namespace AcroMedia\MockMoodle\State;

/**
 * Value object for a group.
 */
final class Group implements \JsonSerializable
{
    /**
     * A list of user IDs.
     *
     * @var array
     */
    private $users;

    /**
     * The group's ID.
     *
     * @var string
     */
    private $id;

    public function __construct(string $id, array $users)
    {
        $this->id = $id;
        $this->users = $users;
    }

    /**
     * Get the group's ID.
     *
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * Whether a user is in the group.
     *
     * @param string $id
     * @return bool
     */
    public function has(string $id): bool
    {
        return in_array($id, $this->users, true);
    }

    /**
     * Add a user to the group.
     *
     * @param string $id
     * @return Group
     */
    public function add(string $id): self
    {
        $this->users[] = $id;
        return $this;
    }

    /**
     * Remove a user from the group.
     *
     * @param string $id
     * @return Group
     */
    public function remove(string $id): self
    {
        foreach ($this->users as $delta => $user) {
            if ($user === $id) {
                unset($this->users[$delta]);
            }
        }
        return $this;
    }

    public function jsonSerialize()
    {
        return $this->users;
    }
}

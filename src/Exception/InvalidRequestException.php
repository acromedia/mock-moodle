<?php

namespace AcroMedia\MockMoodle\Exception;

/**
 * An exception when a request was invalid.
 */
final class InvalidRequestException extends \Exception
{
}

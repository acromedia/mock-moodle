<?php

namespace AcroMedia\MockMoodle\Exception;

/**
 * An exception when the application has invalid state.
 */
final class InvalidStateException extends \Exception
{

}

<?php

namespace AcroMedia\MockMoodle;

use AcroMedia\MockMoodle\Api\MethodLocator;
use Slim\Http\Request;
use Slim\Http\Response;
use AcroMedia\MockMoodle\Api\MoodleApi;

/** @var \Slim\App $app */
// Display the README for requests that aren't to the API.
$app->get('/', function (Request $request, Response $response, array $args) {
    $this->logger->info('/ route');

    return $this->view->render($response, 'html.html', [
        'title' => 'README',
        'body' => $this->markdown->fetch('README.md'),
    ]);
});

$app->get('/api', function (Request $request, Response $response, array $args) {
    $this->logger->info("GET '/api' route");

    $function = $request->getQueryParam('wsfunction');
    if (!$function) {
        $response = $response->withStatus(400, 'You must provide the wsfunction parameter.');
        return $response;
    }

    $result = (new MoodleApi($this->state, new MethodLocator()))->call($function, $request);

    $response = $response->withStatus($result->getStatus());
    return $this->view->render($response, $result->getTemplate(), $result->getArgs());
});

$app->post('/api', function (Request $request, Response $response, array $args) {
    $this->logger->info("POST '/api' route");

    $function = $request->getQueryParam('wsfunction');
    if (!$function) {
        $response = $response->withStatus(400, 'You must provide the wsfunction parameter.');
        return $response;
    }

    $result = (new MoodleApi($this->state, new MethodLocator()))->call($function, $request);
    $this->state->save();

    $response = $response->withStatus($result->getStatus());
    return $this->view->render($response, $result->getTemplate(), $result->getArgs());
});

// Return the system state for debugging or testing.
$app->get('/api/state', function (Request $request, Response $response, array $args) {
    $this->logger->info("GET '/api/state' route");
    $response->getBody()->write($this->state->getJSON());
});

// Reset the application state.
$app->delete('/api/state', function (Request $request, Response $response, array $args) {
    $this->logger->info('Deleting state!');
    $this->state->reset();
    $this->state->save();
});

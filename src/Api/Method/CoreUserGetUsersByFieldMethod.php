<?php

namespace AcroMedia\MockMoodle\Api\Method;

use AcroMedia\MockMoodle\Api\ApiResponse;
use AcroMedia\MockMoodle\State\MoodleState;

/**
 * Handle the get users by field web service.
 *
 * Always returns an empty list since we don't store any fields to look users up by.
 */
final class CoreUserGetUsersByFieldMethod extends MethodBase
{
    /**
     * @var string
     */
    private $template = 'api/empty-list.twig';

    /**
     * @inheritdoc
     */
    public static function appliesTo(): string
    {
        return 'core_user_get_users_by_field';
    }

    /**
     * @inheritdoc
     */
    public function call(MoodleState $state): ApiResponse
    {
        return new ApiResponse(200, [], $this->template);
    }
}

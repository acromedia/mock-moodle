<?php

namespace AcroMedia\MockMoodle\Api\Method;

use AcroMedia\MockMoodle\Api\ApiResponse;
use AcroMedia\MockMoodle\Api\ApiResponseInvalidRequest;
use AcroMedia\MockMoodle\Exception\InvalidRequestException;
use AcroMedia\MockMoodle\State\Constraint\UserExists;
use AcroMedia\MockMoodle\State\MoodleState;

/**
 * Assign a manager to a user.
 */
final class LocalLambdawsAssignManagerMethod extends MethodBase
{
    /**
     * @var string
     */
    private $template = 'api/one-id.twig';

    /**
     * @inheritdoc
     */
    public static function appliesTo(): string
    {
        return 'local_lambdaws_assign_manager';
    }

    /**
     * @inheritdoc
     */
    public function call(MoodleState $state): ApiResponse
    {
        $userId = $this->request->getParsedBodyParam('user_id');
        $managerId = $this->request->getParsedBodyParam('manager_id');

        $errors = [];

        if (!$userId) {
            $errors[] = new InvalidRequestException('You must provide a user_id.');
        }

        if (!$managerId) {
            $errors[] = new InvalidRequestException('You must provide a manager_id.');
        }

        if ($errors) {
            return new ApiResponseInvalidRequest($errors);
        }

        $userExists = new UserExists($state);
        if (!$userExists->satisfied($userId)) {
            $errors[] = new InvalidRequestException(sprintf('%s is not a valid user.', $userId));
        }

        if (!$userExists->satisfied($managerId)) {
            $errors[] = new InvalidRequestException(sprintf('%s is not a valid user.', $managerId));
        }

        if ($errors) {
            return new ApiResponseInvalidRequest($errors);
        }

        return new ApiResponse(200, ['id' => '1'], $this->template);
    }
}

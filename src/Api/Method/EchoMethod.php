<?php

namespace AcroMedia\MockMoodle\Api\Method;

use AcroMedia\MockMoodle\Api\ApiResponse;
use AcroMedia\MockMoodle\State\MoodleState;

/**
 * Handle requests to the echo web service.
 */
final class EchoMethod extends MethodBase
{
    /**
     * @var string
     */
    private $template = 'api/echo.twig';

    /**
     * @inheritdoc
     */
    public static function appliesTo(): string
    {
        return 'echo';
    }

    /**
     * @inheritdoc
     */
    public function call(MoodleState $state): ApiResponse
    {
        return new ApiResponse(200, ['echo' => $this->request->getParsedBodyParam('echo')], $this->template);
    }
}

<?php

namespace AcroMedia\MockMoodle\Api\Method;

use AcroMedia\MockMoodle\Api\ApiResponse;
use AcroMedia\MockMoodle\Api\ApiResponseInvalidRequest;
use AcroMedia\MockMoodle\State\MoodleState;
use AcroMedia\MockMoodle\Exception\InvalidRequestException;

/**
 * Handle the user update web service.
 */
final class CoreUserUpdateUsersMethod extends MethodBase
{
    /**
     * @var string
     */
    private $template = 'api/empty.twig';

    /**
     * @inheritdoc
     */
    public static function appliesTo(): string
    {
        return 'core_user_update_users';
    }

    /**
     * @inheritdoc
     */
    public function call(MoodleState $state): ApiResponse
    {
        $values = $this->request->getParsedBodyParam('users', []);

        if (!$values || !is_array($values)) {
            return new ApiResponseInvalidRequest([new InvalidRequestException('You must provide a list of users.')]);
        }

        return new ApiResponse(200, [], $this->template);
    }
}

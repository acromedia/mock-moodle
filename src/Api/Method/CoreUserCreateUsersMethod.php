<?php

namespace AcroMedia\MockMoodle\Api\Method;

use AcroMedia\MockMoodle\Api\ApiResponse;
use AcroMedia\MockMoodle\Api\ApiResponseInvalidRequest;
use AcroMedia\MockMoodle\State\MoodleState;
use AcroMedia\MockMoodle\Exception\InvalidRequestException;

/**
 * Handle the lambda_sso_create_user web service.
 */
final class CoreUserCreateUsersMethod extends MethodBase
{
    /**
     * @var string
     */
    private $template = 'api/core-user-create-users.twig';

    /**
     * @inheritdoc
     */
    public static function appliesTo(): string
    {
        return 'core_user_create_users';
    }

    /**
     * @inheritdoc
     */
    public function call(MoodleState $state): ApiResponse
    {
        $values = $this->request->getParsedBodyParam('users', []);

        if (!$values || !is_array($values)) {
            return new ApiResponseInvalidRequest([new InvalidRequestException('You must provide a list of users.')]);
        }

        $users = [];

        foreach ($values as $value) {
            $users[] = $state->createUser();
        }

        return new ApiResponse(200, ['users' => $users], $this->template);
    }
}

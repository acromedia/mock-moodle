<?php

namespace AcroMedia\MockMoodle\Api\Method;

use AcroMedia\MockMoodle\Api\ApiResponse;
use AcroMedia\MockMoodle\Api\ApiResponseInvalidRequest;
use AcroMedia\MockMoodle\State\Constraint\UserExists;
use AcroMedia\MockMoodle\State\MoodleState;
use AcroMedia\MockMoodle\Exception\InvalidRequestException;

/**
 * Handle the user deletion web service.
 */
final class CoreUserDeleteUsersMethod extends MethodBase
{
    /**
     * @var string
     */
    private $template = 'api/empty.twig';

    /**
     * @inheritdoc
     */
    public static function appliesTo(): string
    {
        return 'core_user_delete_users';
    }

    /**
     * @inheritdoc
     */
    public function call(MoodleState $state): ApiResponse
    {
        $values = $this->request->getParsedBodyParam('userids', []);

        if (!$values || !is_array($values)) {
            return new ApiResponseInvalidRequest([new InvalidRequestException('You must provide a list of userids.')]);
        }

        $errors = [];

        $userExists = new UserExists($state);
        foreach ($values as $value) {
            if (!$userExists->satisfied($value)) {
                $errors[] = new InvalidRequestException(sprintf('%s is not a valid user.', $value));
            }
        }

        if ($errors) {
            return new ApiResponseInvalidRequest($errors);
        }

        foreach ($values as $value) {
            $state->deleteUser($value);
        }

        return new ApiResponse(200, [], $this->template);
    }
}

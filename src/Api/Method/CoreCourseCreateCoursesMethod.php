<?php

namespace AcroMedia\MockMoodle\Api\Method;

use AcroMedia\MockMoodle\Api\ApiResponse;
use AcroMedia\MockMoodle\Api\ApiResponseInvalidRequest;
use AcroMedia\MockMoodle\Exception\InvalidRequestException;
use AcroMedia\MockMoodle\State\MoodleState;

/**
 * Handle the create course web service.
 */
final class CoreCourseCreateCoursesMethod extends MethodBase
{
    /**
     * @var string
     */
    private $template = 'api/core-course-create-courses.twig';

    /**
     * @inheritdoc
     */
    public static function appliesTo(): string
    {
        return 'core_course_create_courses';
    }

    /**
     * @inheritdoc
     */
    public function call(MoodleState $state): ApiResponse
    {
        $values = $this->request->getParsedBodyParam('courses', []);

        if (!$values || !is_array($values)) {
            return new ApiResponseInvalidRequest([new InvalidRequestException('You must provide a list of courses.')]);
        }

        $courses = [];

        foreach ($values as $value) {
            $courses[] = $state->createCourse($value['shortname'] ?? null);
        }
        return new ApiResponse(200, ['courses' => $courses], $this->template);
    }

}

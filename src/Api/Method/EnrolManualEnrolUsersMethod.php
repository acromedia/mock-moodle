<?php

namespace AcroMedia\MockMoodle\Api\Method;

use AcroMedia\MockMoodle\Api\ApiResponse;
use AcroMedia\MockMoodle\State\Constraint\CourseExists;
use AcroMedia\MockMoodle\State\Constraint\UserExists;
use AcroMedia\MockMoodle\State\MoodleState;
use AcroMedia\MockMoodle\Exception\InvalidRequestException;
use AcroMedia\MockMoodle\Api\ApiResponseInvalidRequest;

/**
 * Enrol users in courses.
 */
final class EnrolManualEnrolUsersMethod extends MethodBase
{

    /**
     * @var string
     */
    private $template = 'api/empty.twig';

    /**
     * @inheritdoc
     */
    public static function appliesTo(): string
    {
        return 'enrol_manual_enrol_users';
    }

    /**
     * @inheritdoc
     */
    public function call(MoodleState $state): ApiResponse
    {
        $values = $this->request->getParsedBodyParam('enrolments', []);
        $errors = [];

        if (!$values) {
            return new ApiResponseInvalidRequest([new InvalidRequestException('You must provide a list of enrolments.')]);
        }

        $userExists = new UserExists($state);
        $courseExists = new CourseExists($state);

        $all_valid = true;
        foreach ($values as $assignment) {
            $valid = true;

            if (empty($assignment['userid'])) {
                $valid = false;
                $errors[] = new InvalidRequestException('Assignments must have a userid.');
            }
            if (empty($assignment['courseid'])) {
                $valid = false;
                $errors[] = new InvalidRequestException('Assignments must have a courseid.');
            }
            if (empty($assignment['roleid'])) {
                $valid = false;
                $errors[] = new InvalidRequestException('Assignments must have a roleid.');
            }

            if (!$valid) {
                $all_valid = false;
                continue;
            }

            if (!$courseExists->satisfied($assignment['courseid'])) {
                $valid = false;
                $errors[] = new InvalidRequestException(sprintf('%s is not a valid course.', $assignment['courseid']));
            }

            if (!$userExists->satisfied($assignment['userid'])) {
                $valid = false;
                $errors[] = new InvalidRequestException(sprintf('%s is not a valid user.', $assignment['userid']));
            }

            if (!$valid) {
                $all_valid = false;
                continue;
            }
        }

        if (!$all_valid) {
            return new ApiResponseInvalidRequest($errors);
        }

        foreach ($values as $assignment) {
            $course = $state->course($assignment['courseid']);
            $course->add($assignment['userid']);
        }

        return new ApiResponse(200, [], $this->template);
    }
}

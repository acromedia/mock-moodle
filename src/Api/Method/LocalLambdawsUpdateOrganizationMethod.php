<?php

namespace AcroMedia\MockMoodle\Api\Method;

use AcroMedia\MockMoodle\Api\ApiResponse;
use AcroMedia\MockMoodle\Api\ApiResponseInvalidRequest;
use AcroMedia\MockMoodle\Exception\InvalidRequestException;
use AcroMedia\MockMoodle\State\MoodleState;

/**
 * Handle the update organizations web service.
 */
final class LocalLambdawsUpdateOrganizationMethod extends MethodBase
{
    /**
     * @var string
     */
    private $template = 'api/one-id.twig';

    /**
     * @inheritdoc
     */
    public static function appliesTo(): string
    {
        return 'local_lambdaws_update_organization';
    }

    /**
     * @inheritdoc
     */
    public function call(MoodleState $state): ApiResponse
    {
        $values = $this->request->getParsedBodyParam('organization', []);

        if (!$values || !is_array($values)) {
            return new ApiResponseInvalidRequest([new InvalidRequestException('You must provide an organization.')]);
        }

        return new ApiResponse(200, ['id' => '1'], $this->template);
    }
}

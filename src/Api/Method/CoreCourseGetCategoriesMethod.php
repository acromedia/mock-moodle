<?php

namespace AcroMedia\MockMoodle\Api\Method;

use AcroMedia\MockMoodle\Api\ApiResponse;
use AcroMedia\MockMoodle\State\MoodleState;

/**
 * Handle the course get web service.
 *
 * Always returns an empty list. Should it return the actual courses instead?
 */
final class CoreCourseGetCategoriesMethod extends MethodBase
{
    /**
     * @var string
     */
    private $template = 'api/empty-list.twig';

    /**
     * @inheritdoc
     */
    public static function appliesTo(): string
    {
        return 'core_course_get_categories';
    }

    /**
     * @inheritdoc
     */
    public function call(MoodleState $state): ApiResponse
    {
        return new ApiResponse(200, [], $this->template);
    }
}

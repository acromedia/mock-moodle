<?php

namespace AcroMedia\MockMoodle\Api\Method;

use AcroMedia\MockMoodle\Api\ApiResponse;
use AcroMedia\MockMoodle\State\MoodleState;

/**
 * Handle the get groups in course web service.
 *
 * Always returns an empty list. Should it return the actual groups instead?
 */
final class CoreGroupGetCourseGroupsMethod extends MethodBase
{
    /**
     * @var string
     */
    private $template = 'api/empty-list.twig';

    /**
     * @inheritdoc
     */
    public static function appliesTo(): string
    {
        return 'core_group_get_course_groups';
    }

    /**
     * @inheritdoc
     */
    public function call(MoodleState $state): ApiResponse
    {
        return new ApiResponse(200, [], $this->template);
    }
}

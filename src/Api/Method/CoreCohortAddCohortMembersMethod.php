<?php

namespace AcroMedia\MockMoodle\Api\Method;

use AcroMedia\MockMoodle\Api\ApiResponse;
use AcroMedia\MockMoodle\Api\ApiResponseInvalidRequest;
use AcroMedia\MockMoodle\Exception\InvalidRequestException;
use AcroMedia\MockMoodle\State\Constraint\UserExists;
use AcroMedia\MockMoodle\State\MoodleState;

/**
 * Add users to a cohort.
 */
final class CoreCohortAddCohortMembersMethod extends MethodBase
{

    /**
     * @var string
     */
    private $template = 'api/core-cohort-add-cohort-members.twig';

    /**
     * @inheritdoc
     */
    public static function appliesTo(): string
    {
        return 'core_cohort_add_cohort_members';
    }

    /**
     * @inheritdoc
     */
    public function call(MoodleState $state): ApiResponse
    {
        $values = $this->request->getParsedBodyParam('members', []);

        if (!$values || !is_array($values)) {
            return new ApiResponseInvalidRequest([new InvalidRequestException('You must provide a list of members.')]);
        }

        $userExists = new UserExists($state);

        $errors = [];
        foreach ($values as $value) {
            $userId = $value['usertype']['value'] ?? null;
            if (!$userId) {
                $errors[] = new InvalidRequestException('Members must have a user id.');
                continue;
            }

            if (!$userExists->satisfied($userId)) {
                $errors[] = new InvalidRequestException(sprintf('%s is not a valid user.', $userId));
            }
        }

        if ($errors) {
            return new ApiResponseInvalidRequest($errors);
        }

        return new ApiResponse(200, [], $this->template);
    }
}

<?php

namespace AcroMedia\MockMoodle\Api\Method;

use AcroMedia\MockMoodle\Api\ApiResponse;
use AcroMedia\MockMoodle\Api\ApiResponseInvalidRequest;
use AcroMedia\MockMoodle\Exception\InvalidRequestException;
use AcroMedia\MockMoodle\State\MoodleState;

/**
 * Handle the create groupings web service.
 */
final class CoreGroupCreateGroupingsMethod extends MethodBase
{
    /**
     * @var string
     */
    private $template = 'api/list-of-ids.twig';

    /**
     * @inheritdoc
     */
    public static function appliesTo(): string
    {
        return 'core_group_create_groupings';
    }

    /**
     * @inheritdoc
     */
    public function call(MoodleState $state): ApiResponse
    {
        $values = $this->request->getParsedBodyParam('groupings', []);

        $groupings = [];

        if (!$values || !is_array($values)) {
            return new ApiResponseInvalidRequest([new InvalidRequestException('You must provide a list of groupings.')]);
        }

        foreach ($values as $delta => $value) {
            $groupings[] = $delta;
        }

        return new ApiResponse(200, ['ids' => $groupings], $this->template);
    }

}

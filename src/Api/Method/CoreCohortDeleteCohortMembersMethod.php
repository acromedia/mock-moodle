<?php

namespace AcroMedia\MockMoodle\Api\Method;

use AcroMedia\MockMoodle\Api\ApiResponse;
use AcroMedia\MockMoodle\Api\ApiResponseInvalidRequest;
use AcroMedia\MockMoodle\Exception\InvalidRequestException;
use AcroMedia\MockMoodle\State\Constraint\UserExists;
use AcroMedia\MockMoodle\State\MoodleState;

/**
 * Remove users from a cohort.
 */
final class CoreCohortDeleteCohortMembersMethod extends MethodBase
{

    /**
     * @var string
     */
    private $template = 'api/empty.twig';

    /**
     * @inheritdoc
     */
    public static function appliesTo(): string
    {
        return 'core_cohort_delete_cohort_members';
    }

    /**
     * @inheritdoc
     */
    public function call(MoodleState $state): ApiResponse
    {
        $values = $this->request->getParsedBodyParam('members', []);

        if (!$values || !is_array($values)) {
            return new ApiResponseInvalidRequest([new InvalidRequestException('You must provide a list of members.')]);
        }

        $userExists = new UserExists($state);

        $errors = [];
        foreach ($values as $value) {
            $userId = $value['userid'] ?? null;
            if (!$userId) {
                $errors[] = new InvalidRequestException('Members must have a userid.');
                continue;
            }

            if (!$userExists->satisfied($userId)) {
                $errors[] = new InvalidRequestException(sprintf('%s is not a valid user.', $userId));
            }
        }

        if ($errors) {
            return new ApiResponseInvalidRequest($errors);
        }

        return new ApiResponse(200, [], $this->template);
    }
}

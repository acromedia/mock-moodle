<?php

namespace AcroMedia\MockMoodle\Api\Method;

use AcroMedia\MockMoodle\Api\ApiResponse;
use AcroMedia\MockMoodle\Api\ApiResponseInvalidRequest;
use AcroMedia\MockMoodle\State\MoodleState;
use AcroMedia\MockMoodle\Exception\InvalidRequestException;

/**
 * Handle the lambda_sso_create_user web service.
 */
final class LambdaSSOCreateUserMethod extends MethodBase
{
    /**
     * @var string
     */
    private $template = 'api/lambda-sso-create-user.twig';

    /**
     * @inheritdoc
     */
    public static function appliesTo(): string
    {
        return 'lambda_sso_create_user';
    }

    /**
     * @inheritdoc
     */
    public function call(MoodleState $state): ApiResponse
    {
        $values = $this->request->getParsedBodyParam('users', []);

        if (!$values || !is_array($values)) {
            return new ApiResponseInvalidRequest([new InvalidRequestException('You must provide a list of users.')]);
        }

        $users = [];

        foreach ($values as $value) {
            $users[] = $state->createUser();
        }

        return new ApiResponse(200, ['users' => $users], $this->template);
    }
}

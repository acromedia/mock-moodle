<?php

namespace AcroMedia\MockMoodle\Api\Method;

use AcroMedia\MockMoodle\Api\ApiResponse;
use AcroMedia\MockMoodle\Api\ApiResponseInvalidRequest;
use AcroMedia\MockMoodle\Exception\InvalidRequestException;
use AcroMedia\MockMoodle\State\MoodleState;

/**
 * Handle the create categories web service.
 */
final class CoreCourseCreateCategoriesMethod extends MethodBase
{
    /**
     * @var string
     */
    private $template = 'api/list-of-ids.twig';

    /**
     * @inheritdoc
     */
    public static function appliesTo(): string
    {
        return 'core_course_create_categories';
    }

    /**
     * @inheritdoc
     */
    public function call(MoodleState $state): ApiResponse
    {
        $values = $this->request->getParsedBodyParam('categories', []);

        $categories = [];

        if (!$values || !is_array($values)) {
            return new ApiResponseInvalidRequest([new InvalidRequestException('You must provide a list of categories.')]);
        }

        foreach ($values as $delta => $value) {
            $categories[] = $delta;
        }

        return new ApiResponse(200, ['ids' => $categories], $this->template);
    }

}

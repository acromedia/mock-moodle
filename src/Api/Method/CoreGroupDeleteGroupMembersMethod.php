<?php

namespace AcroMedia\MockMoodle\Api\Method;

use AcroMedia\MockMoodle\Api\ApiResponse;
use AcroMedia\MockMoodle\Api\ApiResponseInvalidRequest;
use AcroMedia\MockMoodle\Exception\InvalidRequestException;
use AcroMedia\MockMoodle\State\Constraint\GroupExists;
use AcroMedia\MockMoodle\State\Constraint\UserExists;
use AcroMedia\MockMoodle\State\Constraint\UserInGroup;
use AcroMedia\MockMoodle\State\MoodleState;

/**
 * Adds users to groups.
 */
final class CoreGroupDeleteGroupMembersMethod extends MethodBase
{

    /**
     * @var string
     */
    private $template = 'api/empty.twig';

    /**
     * @inheritdoc
     */
    public static function appliesTo(): string
    {
        return 'core_group_delete_group_members';
    }

    public function call(MoodleState $state): ApiResponse
    {
        $values = $this->request->getParsedBodyParam('members', []);
        $errors = [];

        if (!$values) {
            return new ApiResponseInvalidRequest([new InvalidRequestException('You must provide a list of members.')]);
        }

        $groupExists = new GroupExists($state);
        $userExists = new UserExists($state);
        $userInGroup = new UserInGroup($state);

        $all_valid = true;
        foreach ($values as $assignment) {
            $valid = true;

            if (empty($assignment['userid'])) {
                $valid = false;
                $errors[] = new InvalidRequestException('Assignments must have a userid.');
            }
            if (empty($assignment['groupid'])) {
                $valid = false;
                $errors[] = new InvalidRequestException('Assignments must have a groupid.');
            }

            if (!$valid) {
                $all_valid = false;
                continue;
            }

            if (!$groupExists->satisfied($assignment['groupid'])) {
                $valid = false;
                $errors[] = new InvalidRequestException(sprintf('%s is not a valid group.', $assignment['groupid']));
            }

            if (!$userExists->satisfied($assignment['userid'])) {
                $valid = false;
                $errors[] = new InvalidRequestException(sprintf('%s is not a valid user.', $assignment['userid']));
            }

            if (!$valid) {
                $all_valid = false;
                continue;
            }

            $group = $state->group($assignment['groupid']);
            if (!$userInGroup->satisfied($assignment['userid'], $group)) {
                $all_valid = false;
                $errors[] = new InvalidRequestException(sprintf('User %s must first be in group %s.', $assignment['userid'], $group->id()));
            }
        }

        if (!$all_valid) {
            return new ApiResponseInvalidRequest($errors);
        }

        foreach ($values as $assignment) {
            $group = $state->group($assignment['groupid']);
            $group->remove($assignment['userid']);
        }

        return new ApiResponse(200, [], $this->template);
    }
}

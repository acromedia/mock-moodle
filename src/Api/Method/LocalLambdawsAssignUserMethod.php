<?php

namespace AcroMedia\MockMoodle\Api\Method;

use AcroMedia\MockMoodle\Api\ApiResponse;
use AcroMedia\MockMoodle\Api\ApiResponseInvalidRequest;
use AcroMedia\MockMoodle\Exception\InvalidRequestException;
use AcroMedia\MockMoodle\State\Constraint\UserExists;
use AcroMedia\MockMoodle\State\MoodleState;

/**
 * Assign a user to an organization.
 */
final class LocalLambdawsAssignUserMethod extends MethodBase
{
    /**
     * @var string
     */
    private $template = 'api/one-id.twig';

    /**
     * @inheritdoc
     */
    public static function appliesTo(): string
    {
        return 'local_lambdaws_assign_user';
    }

    /**
     * @inheritdoc
     */
    public function call(MoodleState $state): ApiResponse
    {
        $userId = $this->request->getParsedBodyParam('user_id');

        if (!$userId) {
            return new ApiResponseInvalidRequest([new InvalidRequestException('You must provide a user_id.')]);
        }

        if (!(new UserExists($state))->satisfied($userId)) {
            return new ApiResponseInvalidRequest([new InvalidRequestException(sprintf('%s is not a valid user.', $userId))]);
        }

        return new ApiResponse(200, ['id' => '1'], $this->template);
    }
}

<?php

namespace AcroMedia\MockMoodle\Api\Method;

use AcroMedia\MockMoodle\Api\ApiResponse;
use AcroMedia\MockMoodle\Api\ApiResponseInvalidRequest;
use AcroMedia\MockMoodle\State\Constraint\GroupExists;
use AcroMedia\MockMoodle\State\MoodleState;
use AcroMedia\MockMoodle\Exception\InvalidRequestException;

/**
 * Handle the assign grouping web service.
 */
final class CoreGroupAssignGroupingMethod extends MethodBase
{
    /**
     * @var string
     */
    private $template = 'api/empty.twig';

    /**
     * @inheritdoc
     */
    public static function appliesTo(): string
    {
        return 'core_group_assign_grouping';
    }

    /**
     * @inheritdoc
     */
    public function call(MoodleState $state): ApiResponse
    {
        $errors = [];
        $values = $this->request->getParsedBodyParam('assignments', []);
        $groupExists = new GroupExists($state);

        if (!$values) {
            $errors[] = new InvalidRequestException('You must provide a list of assignments.');
        }

        foreach ($values as $value) {
            if (empty($value['groupid'])) {
                $errors[] = new InvalidRequestException('Assignments must have a groupid.');
            } elseif (!$groupExists->satisfied($value['groupid'])) {
                $errors[] = new InvalidRequestException(sprintf('%s is not a valid group.', $value['groupid']));
            }
        }

        if ($errors) {
            return new ApiResponseInvalidRequest($errors);
        }

        return new ApiResponse(200, [], $this->template);
    }
}

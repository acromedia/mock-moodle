<?php

namespace AcroMedia\MockMoodle\Api\Method;

use AcroMedia\MockMoodle\Api\ApiResponse;
use AcroMedia\MockMoodle\Api\ApiResponseInvalidRequest;
use AcroMedia\MockMoodle\State\Constraint\CourseExists;
use AcroMedia\MockMoodle\State\MoodleState;
use AcroMedia\MockMoodle\Exception\InvalidRequestException;

/**
 * Handle the group create web service.
 */
final class CoreGroupCreateGroupsMethod extends MethodBase
{
    /**
     * @var string
     */
    private $template = 'api/core-group-create-groups.twig';

    /**
     * @inheritdoc
     */
    public static function appliesTo(): string
    {
        return 'core_group_create_groups';
    }

    /**
     * @inheritdoc
     */
    public function call(MoodleState $state): ApiResponse
    {
        $errors = [];
        $groups = [];
        $values = $this->request->getParsedBodyParam('groups', []);
        $courseExists = new CourseExists($state);

        if (!$values) {
            $errors[] = new InvalidRequestException('You must provide a list of groups.');
        }

        foreach ($values as $value) {
            if (empty($value['courseid'])) {
                $errors[] = new InvalidRequestException('Groups must have a courseid.');
            } elseif (!$courseExists->satisfied($value['courseid'])) {
                $errors[] = new InvalidRequestException(sprintf('%s is not a valid course.', $value['courseid']));
            } else {
                $groups[] = $state->createGroup($state->course($value['courseid']));
            }
        }

        if ($errors) {
            return new ApiResponseInvalidRequest($errors);
        }

        return new ApiResponse(200, ['groups' => $groups], $this->template);
    }
}

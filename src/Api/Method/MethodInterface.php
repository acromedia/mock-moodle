<?php

namespace AcroMedia\MockMoodle\Api\Method;

use AcroMedia\MockMoodle\Api\ApiResponse;
use AcroMedia\MockMoodle\State\MoodleState;
use Slim\Http\Request;

/**
 * Defines an API method.
 */
interface MethodInterface
{

    public function __construct(Request $request);

    /**
     * Returns the method to be called for.
     */
    public static function appliesTo(): string;

    /**
     * Handles a request.
     *
     * @param MoodleState $state
     * @return ApiResponse
     */
    public function call(MoodleState $state): ApiResponse;
}

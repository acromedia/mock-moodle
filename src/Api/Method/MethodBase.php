<?php

namespace AcroMedia\MockMoodle\Api\Method;

use Slim\Http\Request;

abstract class MethodBase implements MethodInterface
{
    /**
     * @var Request
     */
    protected $request;


    public function __construct(Request $request)
    {
        $this->request = $request;
    }
}

<?php

namespace AcroMedia\MockMoodle\Api;

/**
 * The result of a request to a web service.
 */
class ApiResponse
{

    /**
     * An HTTP status code.
     *
     * @var int
     */
    protected $status;

    /**
     * @var array
     */
    protected $args;

    /**
     * @var string
     */
    protected $template;

    /**
     * ApiResponse constructor.
     *
     * @param int $status
     *   An HTTP status code.
     * @param array $args
     *   Arguments to the response template.
     * @param string $template
     *   The path to a template for the response.
     */
    public function __construct(int $status, array $args, string $template)
    {
        $this->status = $status;
        $this->args = $args;
        $this->template = $template;
    }

    /**
     * @param int $status
     *   An HTTP status code.
     * @return ApiResponse
     */
    public function withStatus(int $status)
    {
        return new static($status, $this->args, $this->template);
    }

    /**
     * @param array $args
     *   Arguments for the template.
     * @return ApiResponse
     */
    public function withArgs(array $args): ApiResponse
    {
        return new static($this->status, $args, $this->template);
    }

    /**
     * @return int
     *   An HTTP status code.
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return array
     *   Arguments for the template.
     */
    public function getArgs(): array
    {
        return $this->args;
    }

    /**
     * @return string
     *   The path to a template file.
     */
    public function getTemplate(): string
    {
        return $this->template;
    }
}

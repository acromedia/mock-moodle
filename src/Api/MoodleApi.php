<?php

namespace AcroMedia\MockMoodle\Api;

use Slim\Http\Request;
use AcroMedia\MockMoodle\State\MoodleState;

/**
 * Receives requests to Moodle web services and delivers the response.
 */
final class MoodleApi
{

    /**
     * @var MoodleState
     */
    private $state;

    /**
     * A service for locating API methods.
     *
     * @var MethodLocator
     */
    private $locator;

    public function __construct(MoodleState $state, MethodLocator $locator)
    {
        $this->state = $state;
        $this->locator = $locator;
    }

    /**
     * @param string $method
     * @param Request $request
     * @return ApiResponse
     */
    public function call(string $method, Request $request): ApiResponse
    {
        return $this->locator->getMethod($method, $request)->call($this->state);
    }
}

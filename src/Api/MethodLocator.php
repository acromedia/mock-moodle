<?php

namespace AcroMedia\MockMoodle\Api;

use AcroMedia\MockMoodle\Api\Method\MethodInterface;
use Slim\Http\Request;

/**
 * Finds available handlers and the web service methods they apply to.
 */
final class MethodLocator
{
    /**
     * A list of MethodInterface implementations, keyed by the web service function they apply to.
     *
     * @var string[]
     */
    private $classes;

    /**
     * Load installed implementations and the method they apply to.
     */
    public function __construct()
    {
        $files = scandir(__DIR__ . '/Method', SCANDIR_SORT_NONE);
        foreach ($files as $file) {
            if (preg_match('/(.*Method)\.php$/', $file, $matches)) {
                include_once __DIR__ . '/Method/' . $file;
                /** @var MethodInterface $class */
                $class = '\\AcroMedia\\MockMoodle\\Api\\Method\\' . $matches[1];
                $this->classes[$class::appliesTo()] = $class;
            }
        }
    }

    /**
     * Get the handler for a web service method.
     *
     * @param string $method
     *   The name of the web service function.
     * @param Request $request
     * @return MethodInterface
     */
    public function getMethod(string $method, Request $request): MethodInterface
    {
        $class = $this->getImplementation($method);
        return new $class($request);
    }

    /**
     * Get the class that implements a method.
     *
     * @param string $method
     *   The name of an API method.
     * @return string
     *   The name of a class that implements the requested method.
     *
     * @throws \InvalidArgumentException
     *   If there's no implementation found for that method.
     */
    public function getImplementation(string $method): string
    {
        if (!isset($this->classes[$method])) {
            throw new \InvalidArgumentException(sprintf('%s is not a discovered method', $method));
        }
        return $this->classes[$method];
    }

}

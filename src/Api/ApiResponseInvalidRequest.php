<?php

namespace AcroMedia\MockMoodle\Api;

use AcroMedia\MockMoodle\Exception\InvalidRequestException;

/**
 * The result of an invalid request to a web service.
 */
final class ApiResponseInvalidRequest extends ApiResponse
{

    /**
     * ApiResponseInvalidRequest constructor.
     *
     * @param InvalidRequestException[] $errors
     *   Errors from the request.
     */
    public function __construct(array $errors)
    {
        parent::__construct(400, ['errors' => $errors], 'api/invalid-parameter-values.twig');
    }
}

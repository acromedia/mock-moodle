<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // Set to false in production.
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header.

        // twig-view settings.
        'view' => [
            'template_path' => __DIR__ . '/../templates/',
            // Set to true in production.
            'cache' => false,
            'cache_path' => __DIR__ . '/../templates/cache',
        ],

        // Monolog settings.
        'logger' => [
            'name' => 'mock-totara',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
        'state' => [
            // State is saved here between requests.
            'path' => '../state/state.json',
            // State to use in PHPUnit to avoid overwriting application state during tests.
            'test_path' => '../state/test-state.json',
        ],
    ],
];

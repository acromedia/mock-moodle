# Template caching

Cache files for twig templates will be stored in this directory.

## Clearing caches

To clear twig's caches, delete all of the directories it has created here.

```
$ ls templates/cache
e4  README.md
$ rm -r templates/cache/e4
```
